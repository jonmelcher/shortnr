import sqlite3
import string
import re
import random
from flask import Flask
from flask import jsonify
from flask import redirect
app = Flask(__name__)


def fetch_from_db(query, tpl):
    conn = sqlite3.connect('urls.db')
    cursor = conn.cursor()
    cursor.execute(query, tpl)
    result = cursor.fetchone()
    conn.close()
    return result if result is None else result[0]

def store_in_db(query, tpl):
    conn = sqlite3.connect('urls.db')
    cursor = conn.cursor()
    cursor.execute(query, tpl)
    conn.commit()
    conn.close()
    return

def store_url_in_db(url):
    to_store = ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(5))
    url = re.sub(r"^http(s)?:", '', url)
    store_in_db('INSERT OR REPLACE INTO urls (short_url, url) VALUES (?, ?)', (to_store, url,))
    return to_store

def fetch_url_from_db(short_url):
    return fetch_from_db('SELECT url FROM urls WHERE short_url=?', (short_url,))

def fetch_short_url_from_db(url):
    return fetch_from_db('SELECT short_url FROM urls WHERE url=?', (url,))

@app.route('/')
@app.route('/<string:short_url>')
def redirect_to_url(short_url = None):
    stored = fetch_url_from_db(short_url)
    if stored is None:
        return jsonify({
            'status_code': 400,
            'error': 'Invalid shortened URL provided'
        }), 400
    return redirect(stored, code = 302);

@app.route('/shortnr/')
@app.route('/shortnr/<path:url>')
def shortnr(url = None):
    if url is None:
        return jsonify({
            'status_code': 400,
            'error': 'No URL provided'
        }), 400
    stored = fetch_short_url_from_db(url)
    if stored is None:
        stored = store_url_in_db(url)
    return jsonify({
        'status_code': 200,
        'url': stored
    }), 200

if __name__ == "__main__":
    app.run()

