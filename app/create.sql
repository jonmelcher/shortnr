DROP TABLE IF EXISTS urls;
CREATE TABLE urls(short_url char(5) PRIMARY KEY, url text);

